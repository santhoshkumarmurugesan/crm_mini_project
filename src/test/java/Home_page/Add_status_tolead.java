package Home_page;

import org.openqa.selenium.By;

import POM.Add_status_pom;
import crm_automate.singleton_page;
import wait.driverwait;

public class Add_status_tolead {


	public static void click_add_status() throws InterruptedException {
		
		
		singleton_page.wait_driver(By.xpath("//div[text()='Customer Details Saved Successfully.. ']"));
		driverwait.sleep3();
		Add_status_pom.addstatus.click();
	}



	public static void click_new_btn() throws InterruptedException
	{
		driverwait.sleep5();
		Add_status_pom.clicknew.click();
	}


	public static void Accept_Status() throws InterruptedException {

		driverwait.sleep1();
		Add_status_pom.acceptstatus.click();

	
	}
}


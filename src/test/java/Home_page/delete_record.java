package Home_page;

import org.openqa.selenium.By;

import POM.delete_record_pom;
import crm_automate.singleton_page;
import wait.driverwait;

public class delete_record {

	public static void comeback_to_lead() throws InterruptedException
	{
		
		singleton_page.wait_elem_visible(By.xpath("(//td/span[@data-toggle='tooltip'])[2]"));
		driverwait.sleep1();
		
		delete_record_pom.comestolead.click();	
		
	}
	
	
	public static void delete_records() throws InterruptedException
	{
		driverwait.sleep3();
		delete_record_pom.delete.click();
		driverwait.sleep3();
		singleton_page.driver_quit();
	}
	
	
	


	
}

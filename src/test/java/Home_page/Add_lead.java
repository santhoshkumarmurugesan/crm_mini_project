package Home_page;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import POM.add_lead_pom;
import crm_automate.singleton_page;
import wait.driverwait;

public class Add_lead {
	

	
	public static WebDriver driver = singleton_page.driver;



//	public static void write_name(String name) throws InterruptedException 
//	{
//
//		  singleton_page.wait_elem_visible(By.xpath("(//span[text()='First Name'])[2]"));
//		add_lead_pom.addname.sendKeys(name);
//
//	}
//	
//	public static void add_lead_owner(String owner) throws InterruptedException
//	{
//		driverwait.sleep1();
//		add_lead_pom.add_lead_owner.sendKeys(owner);
//	}
	
	public static void enter_name_phone_owner(String name, String owner, String phno) throws InterruptedException
	{
		singleton_page.wait_elem_visible(By.xpath("(//span[text()='First Name'])[2]"));
		add_lead_pom.addname.sendKeys(name);
		driverwait.sleep1();
		add_lead_pom.add_lead_owner.sendKeys(owner);
		driverwait.sleep1();
		add_lead_pom.addphno.sendKeys(phno);
	}
	
	
	
	
	
	
	
	public static void dropdown_1() throws InterruptedException
	{
		add_lead_pom.drop_1.click();
		driverwait.sleep2();
		
		driver.findElement(By.xpath("//option[text()='Advertisement']")).click();
		
		
	}
	
	
	public static void dropdown_2() throws InterruptedException
	{
		add_lead_pom.drop_2.click();
		driverwait.sleep2();
		driver.findElement(By.xpath("//option[text()='Passenger Rail']")).click();
		
	}
	
	
	
	
	public static void update_details(String revenue, String company, String email, String tweet, String street, String state, String zipcode, String alter, String city) throws InterruptedException
	{
		
		
		
		driverwait.sleep1();
		add_lead_pom.Annual_income.sendKeys(revenue);
		driverwait.sleep1();
		add_lead_pom.addcompany.sendKeys(company);
		driverwait.sleep1();
		add_lead_pom.addemail.sendKeys(email);
		driverwait.sleep1();
		add_lead_pom.twitter.sendKeys(tweet);
		driverwait.sleep1();
		add_lead_pom.street.sendKeys(street);
		driverwait.sleep1();
		add_lead_pom.state.sendKeys(state);
		driverwait.sleep1();
		add_lead_pom.zip_code.sendKeys(zipcode);
		driverwait.sleep1();
		add_lead_pom.addanother.sendKeys(alter);
		driverwait.sleep1();
		add_lead_pom.addcity.sendKeys(city);
		
		
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
//	
//	public static void revenue(String revenue) throws InterruptedException
//	{
//		add_lead_pom.Annual_income.sendKeys(revenue);
//	}
	
	
//	public static void write_number(String phno) throws InterruptedException
//	{
//		driverwait.sleep1();
//		add_lead_pom.addphno.sendKeys(phno);
//	}
	
	
//	public static void Add_company(String company) throws InterruptedException
//	{
//		driverwait.sleep1();
//		add_lead_pom.addcompany.sendKeys(company);
//	}
	
//	public static void write_email(String email) throws InterruptedException
//	{
//		driverwait.sleep1();
//		add_lead_pom.addemail.sendKeys(email);
//	}
	
	
//	public static void tweet(String tweet)throws InterruptedException
//	{
//		driverwait.sleep1();
//		add_lead_pom.twitter.sendKeys(tweet);
//	}
	
	
//	public static void street(String street)throws InterruptedException
//	{
//		driverwait.sleep1();
//		add_lead_pom.street.sendKeys(street);
//		
//	}
	
//	public static void Add_state(String state)throws InterruptedException
//	{
//		driverwait.sleep1();
//		add_lead_pom.state.sendKeys(state);
//	}
	
	
//	public static void zipcode(String zipcode)throws InterruptedException
//	{
//		driverwait.sleep1();
//		add_lead_pom.zip_code.sendKeys(zipcode);
//	}
	
	
	
	
	
//	public static void write_another_mail(String alter) throws InterruptedException
//	{
//		driverwait.sleep1();
//		add_lead_pom.addanother.sendKeys(alter);
//	}
	
//	public static void Add_city(String city) throws InterruptedException
//	{
//		driverwait.sleep1();
//		add_lead_pom.addcity.sendKeys(city);
//		
//	}
	

	
	public static void click_to_submit() throws InterruptedException
	{
		
		JavascriptExecutor js=(JavascriptExecutor)driver;
		js.executeScript("window. scrollBy(0,250)");
		
		add_lead_pom.submit.click();
		
	}
	
	
	
}

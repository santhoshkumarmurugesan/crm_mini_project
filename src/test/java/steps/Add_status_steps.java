package steps;

import Home_page.Add_status_tolead;
import io.cucumber.java.en.*;

public class Add_status_steps {

	@Given("Add status for the lead")
	public void add_status_for_the_lead() throws InterruptedException {

		Add_status_tolead.click_add_status();
	}

	@Then("click new btn")
	public void click_new_btn() throws InterruptedException {

		Add_status_tolead.click_new_btn();
	}

	@Then("accept status")
	public void accept_status() throws InterruptedException {


		Add_status_tolead.Accept_Status();
	}

	
}

package steps;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import driven.read_excel;
import io.cucumber.java.en.*;
import login.Auto_login;

public class loginstepdef {

	@Given("initiate chrome and goto the CRM url")
	public void initiate_chrome_and_goto_the_crm_url() {

		Auto_login.go_to_url();

	}

//	@And("enter the correct id and pass from <rowNumber>")
//	public void enter_the_correct_id_and_pass_from_row_number(int rowNumber, String location) throws IOException {
//
//		List<HashMap<String, String>> input = read_excel.xl(location);
//		input.get(rowNumber).get("user");
//		input.get(rowNumber).get("pass");
//	}

	@And("enter the correct id and pass from {int} exel {string}")
	public void enter_the_correct_id_and_pass_from_exel(int rowNumber, String location)
			throws IOException, InterruptedException{
		List<HashMap<String, String>> input = read_excel.xl(location);
		Auto_login.enter_crednetials(input.get(rowNumber).get("user"), input.get(rowNumber).get("pass"));
		
		
		
	}
	
	
	
	
//	@And("enter the correct {string} id")
//	public void enter_the_correct_username_id(String user1) throws InterruptedException {
//Integer int1, String string,
//		Auto_login.enter_username(user1);
//	}

	@Then("click the Submit btn")
	public void click_the_submit_btn() throws InterruptedException {

		Auto_login.submit();
	}

}

package steps;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import Home_page.*;
import driven.read_excel;
import io.cucumber.java.en.*;
import login.Auto_login;

public class Add_lead_steps {

//	@Given("addname {string}")
//	public void addname(String name) throws InterruptedException {
//
//		Add_lead.write_name(name);
//	}
//
//	
//	 @And("add lead {string}")
//	 public void add_lead_owner(String owner)throws InterruptedException{
//		 Add_lead.add_lead_owner(owner);
//	 }
//
//	@And("add phonenumber {string}")
//	public void add_phonenumber(String phno) throws InterruptedException {
//
//		Add_lead.write_number(phno);
//	}

	@Given("Add name and lead and phonenumber from {int} exel {string}")
	public void Add_name_and_lead_and_phonenumber_from_exel(int rowNumber, String location) throws IOException, InterruptedException
	{
		
		List<HashMap<String, String>> input = read_excel.xl(location);
		Add_lead.enter_name_phone_owner(
				input.get(rowNumber).get("name"),
				input.get(rowNumber).get("owner"),
				input.get(rowNumber).get("phno"));
	}
	

	 @And("dropdown_1")
	 public void dropdown_1() throws InterruptedException{
		 Add_lead.dropdown_1();
	 }
	
	 @And("dropdown_2")
	 public void dropdown_2() throws InterruptedException{
		 Add_lead.dropdown_2();
	 }
	 
	 
	 @Given("enter next deatails from {int} exel {string}")
	 public void enter_next_deatails_from_exel(int rowNumber, String location) throws IOException, InterruptedException {
		 List<HashMap<String, String>> input = read_excel.xl(location);
		 Add_lead.update_details(
		 input.get(rowNumber).get("revenue"),
		 input.get(rowNumber).get("company"),
		 input.get(rowNumber).get("email"),
		 input.get(rowNumber).get("tweet"),
		 input.get(rowNumber).get("street"),
		 input.get(rowNumber).get("state"),
		 input.get(rowNumber).get("zipcode"),
		 input.get(rowNumber).get("alter"),
		 input.get(rowNumber).get("city"));
		 
		 
	 }
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
//	 
//	 @And("add revenue {string}")
//	 public void revenue(String revenue)throws InterruptedException{
//	 
//		 Add_lead.revenue(revenue);
//	 }
//	 
//	 
//	@And("add company {string}")
//	public void add_company(String company) throws InterruptedException {
//		Add_lead.Add_company(company);
//
//	}
//
//	@And("add email {string}")
//	public void add_email(String email) throws InterruptedException {
//
//		Add_lead.write_email(email);
//	}
//
//	
//	@And("add tweet {string} write")
//	public void twitter_write(String tweet)throws InterruptedException {
//		Add_lead.tweet(tweet);
//	}
//	
//	@And("add street {string}")
//	public void add_street(String street)throws InterruptedException {
//	
//		Add_lead.street(street);
//	}
//	
//	 @And("add state {string}")
//	 public void add_state(String state)throws InterruptedException{
//		 Add_lead.Add_state(state);
//	 }
//	
//	 @And("add zipcode {string}")
//	public void add_zipcode(String zipcode)throws InterruptedException{
//		 Add_lead.zipcode(zipcode);
//	 }
//	
//	
//	
//	
//	@Then("add alter {string} mail")
//	public void add_another_mail(String alter) throws InterruptedException {
//		Add_lead.write_another_mail(alter);
//
//	}
//
//	@And("add city {string} name")
//	public void add_city_name(String city) throws InterruptedException {
//
//		Add_lead.Add_city(city);
//	}
	
	
	@Then("click to submit details")
	public void click_to_submit_details() throws InterruptedException {
		
		Add_lead.click_to_submit();

	}
}

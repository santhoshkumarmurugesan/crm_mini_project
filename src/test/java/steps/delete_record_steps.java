package steps;

import Home_page.delete_record;
import io.cucumber.java.en.*;

public class delete_record_steps {
	
	
	@Given("comes to lead")
	public void comes_to_lead() throws InterruptedException {
		delete_record.comeback_to_lead();
	}

	@And("click to delete the save record")
	public void click_to_delete_the_save_record() throws InterruptedException {
		delete_record.delete_records();
	}

}

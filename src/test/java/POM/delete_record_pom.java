package POM;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class delete_record_pom {

	
	@FindBy(xpath =("//span[text()='Lead']"))
	public static WebElement comestolead;
	
	
	@FindBy(xpath = ("(//span[contains(@class,'fa fa-trash')])[1]"))
	public static WebElement delete;
	
	
}

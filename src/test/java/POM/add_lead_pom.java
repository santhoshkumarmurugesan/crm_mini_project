package POM;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class add_lead_pom {

	
	@FindBy(xpath=("(//input[@name='value2'])[1]"))
	public static WebElement addname;
	
	
	
	@FindBy(xpath=("(//input[@id='ex1_value'])[2]"))
	public static WebElement add_lead_owner;
	
	
	@FindBy(xpath=("(//select[@id='select3'])[1]"))
	public static WebElement drop_1;
	
	@FindBy(xpath=("(//select[@id='select4'])[1]"))
	public static WebElement drop_2;
	
	
	@FindBy(xpath =("((//span[text()='Annual Revenue'])[2]/following::input)[2]"))
	public static WebElement Annual_income; 

	
	@FindBy(xpath = ("//input[@id='phone123']"))
	public static WebElement addphno;
	
	
	
	@FindBy(xpath = ("(//input[@name='value6'])[1]"))	
	public static WebElement addcompany;
	
	
	@FindBy(xpath = ("(//input[@name='value8'])[1]"))
	public static WebElement addemail;
	
	
	
	
	@FindBy(xpath=("((//span[text()='Twitter'])[2]/following::input)[1]"))
	public static WebElement twitter;
	
	
	@FindBy(xpath = ("((//span[text()='Street'])[2]/following::input)[1]"))
	public static WebElement street;
	
	
	@FindBy(xpath = ("((//span[text()='State'])[2]/following::input)[1]"))
	public static WebElement state;
	
	@FindBy(xpath=("((//span[text()='Zip Code'])[2]/following::input)[1]"))
	public static WebElement zip_code;
	
	
	
	
	
	
	
	
	
	
	@FindBy(xpath = ("(//input[@name='value8'])[3]"))
	public static WebElement addanother;

	
	
	@FindBy(xpath = ("((//span[text()='City'])[2]/following::input)[1]"))
	public static WebElement addcity;
	
	
	@FindBy(xpath = ("//button[@id='saveCustomer']"))
	public static WebElement submit;



	
//	@FindBy(xpath = ("//textarea[@tabindex='101']"))
//	public static WebElement adddescription;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}

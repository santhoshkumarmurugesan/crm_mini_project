package crm_automate;

import java.io.File;
import java.io.IOException;
import java.time.Duration;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

//import com.aventstack.extentreports.ExtentReports;
//import com.aventstack.extentreports.ExtentTest;
//import com.aventstack.extentreports.reporter.ExtentSparkReporter;

import POM.*;
import io.github.bonigarcia.wdm.WebDriverManager;

public class singleton_page 
{
//	public static ExtentReports extentreport;
//	public static ExtentSparkReporter sparkreporter;
//	public static ExtentTest testcase;
	
	public static WebDriver driver=null;
	public static void init_driver() 
	{
//		extentreport =new ExtentReports();
//		sparkreporter =new ExtentSparkReporter("target/spark.html");
//		extentreport.attachReporter(sparkreporter);
        
		if (driver==null) 
		{
			WebDriverManager.chromedriver().setup();
			driver=new ChromeDriver();
			driver.manage().window().maximize();

		}
		PageFactory.initElements(driver, login_pom.class);
		PageFactory.initElements(driver, lead_pom.class);
		PageFactory.initElements(driver, add_lead_pom.class);
		PageFactory.initElements(driver, Add_status_pom.class);
		PageFactory.initElements(driver, lead_to_acc_pom.class);
		PageFactory.initElements(driver, delete_record_pom.class);
		
		
	}



	public static void driver_close() {

		driver.close();
	}


	public static void driver_quit() {
		driver.quit();
	}


	public static void wait_driver(By locator)
	{
		WebDriverWait wait=new WebDriverWait(driver, Duration.ofSeconds(20));
		
		wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
	}

	public static void wait_elem_visible(By locator)
	{
		WebDriverWait wait=new WebDriverWait(driver, Duration.ofSeconds(20));
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
	}


	public static void screenshot() throws IOException {
		
		File pic = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE) ;
		FileUtils.copyFile(pic, new File("image.png")); 
		
		
	}
	
	
	
	
	
	
	


}

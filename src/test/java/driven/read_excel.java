package driven;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import crm_automate.singleton_page;

public class read_excel{
	
	 static String xl_location="C:\\Users\\CBT\\eclipse-workspace\\CustomerResourceManagement\\excel_files\\Book.xlsx";

	 
	public static List<HashMap<String, String>> xl(String xl_location) throws IOException {

		List<HashMap<String, String>> input = new ArrayList<HashMap<String, String>>();
		FileInputStream file1 =new FileInputStream(xl_location);

		try (XSSFWorkbook workbook = new XSSFWorkbook(file1)) {
			XSSFSheet sheet =workbook.getSheet("Sheet1");
			
			List<List<String>> listvalue = new ArrayList<List<String>>();
			
			
			
			for (int row=0; row<=sheet.getLastRowNum(); row++ ) {
				
				
				List<String> list=new ArrayList<String>();
				
				for(int column=0; column<sheet.getRow(row).getLastCellNum(); column++) {
					
					String key=sheet.getRow(row).getCell(column).toString();
					
					list.add(key);
					
				}
				listvalue.add(list);
			
			}
		
			
			List<String> title=listvalue.get(0);
			for(int i=1; i<listvalue.size(); i++) {
				List<String> value=listvalue.get(i);
				HashMap<String, String> map = new LinkedHashMap<String, String>();
				for (int j = 0; j < title.size(); j++) {
					map.put(title.get(j), value.get(j));
				}
				input.add(map);
				
				
			}
			
		
			
//for (HashMap<String, String> list1 : input) {
//				
//				System.out.println(list1);
//				
//			}
			
				
			
		}
		
		return input;

		
		
	}



//public static void main(String[] args) throws IOException {
//	
//	xl(xl_location);
//}


}

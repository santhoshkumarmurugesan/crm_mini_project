package runner_package;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = "src/test/resources/CRMfilefeature/CRM_login.feature",
		glue = "steps",
		plugin={"pretty"
				},
		publish=true
		)
public class CRM_runner {

}

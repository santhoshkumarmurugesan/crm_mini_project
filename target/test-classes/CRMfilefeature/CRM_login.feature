Feature: automate CRM

  Scenario Outline: login to CRM page
    Given initiate chrome and goto the CRM url
    And enter the correct id and pass from <rowNumber> exel "C:\\Users\\CBT\\eclipse-workspace\\CustomerResourceManagement\\excel_files\\Book.xlsx"
    Then click the Submit btn

    Examples: 
      | rowNumber |
      |    0      |

  Scenario: from home goto lead to add new lead
    Given goto the lead
    And create new lead

  Scenario Outline: To Add new lead enter follow details
    Given Add name and lead and phonenumber from <rowNumber> exel "C:\\Users\\CBT\\eclipse-workspace\\CustomerResourceManagement\\excel_files\\Book.xlsx"
    
    Examples:
    | rowNumber |
    |    0      |
  
  
   Scenario Outline: details of lead
   Given enter next deatails from <rowNumber> exel "C:\\Users\\CBT\\eclipse-workspace\\CustomerResourceManagement\\excel_files\\Book.xlsx"
   Then click to submit details
   Examples:
   | rowNumber |
   |     0     |
    Scenario: Add status to the lead
    Given Add status for the lead
    Then click new btn
    And accept status

  Scenario: covert lead as contact to accountant
    Given click convert So lead change as contact to accountant
    And confrim the convert

  Scenario: comeback to lead and delete record
    Given comes to lead
    And click to delete the save record
   
   